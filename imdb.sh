#!/bin/bash

# Returns a movie's page on IMDB when given its name

[[ $# -lt 1 ]] && echo "$0  ‹a movie's name›" && exit 1


# From http://stackoverflow.com/a/10660730/1418165
function urlencode() {
    local string="$1"
    local strlen=${#string}
    local encoded=''

    for (( pos=0 ; pos<strlen ; pos++ )); do
        c=${string:$pos:1}
        case "$c" in
            [-_.~a-zA-Z0-9])
                o="$c" ;;
            *)
                printf -v o '%%%02x' "'$c" ;;
        esac
        encoded+="$o"
    done
    echo $encoded
}

function urldecode() {
    printf -v REPLY '%b' "${1//%/\\x}"
    echo $REPLY
}

function get() {
    local out="$1" ; shift
    local UA="Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.152 Safari/537.22"

    which 'curl' >/dev/null
    if [ $? -eq 0 ]; then
        curl --silent -f -o "$out" -A "$UA" -L -S $*
    else
        wget --quiet     -O "$out" -U "$UA"       $*
    fi
}


function imdb() {
    local name="$1" ; local tmp=/tmp/$RANDOM.htm
    local encoded ; local imdb ; local title

    touch $tmp
    encoded=$(urlencode "$name")
    get $tmp 'https://www.google.com/search?q=site%3Aimdb.com+inurl%3Atitle+'"$encoded"'&oq=site%3Aimd&ie=UTF-8'
    imdb=$(grep -Po 'imdb\.com/title/tt[^/]+/' $tmp | head -n 1)
    [[ "$imdb" = '' ]] && exit 2
    imdb=${imdb##*imdb.com/}
    imdb="http://www.imdb.com/${imdb}"
    get $tmp $imdb
    title=$(grep -P 'og:title' $tmp | grep -Po '"[^"]+"')
    COLUMNS=$(tput cols)
    printf "%s%$(($COLUMNS -${#imdb}))s\n" "$imdb" "$title"
    rm -f $tmp
}

imdb "$*"
