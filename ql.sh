#!/bin/bash

# Call `imdb.sh` on files then write an Internet shortcut here.

function shortcut() {
    local path="$1"
    local url="$2"

    case `uname` in
        Darwin)
            path="$path".webloc
            cat <<EOF > "$path"
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
<key>URL</key>
<string>${url}</string>
</dict>
</plist>
EOF
            echo "Wrote '$path'" ;;
        *)
            echo "Internet Shortcut file not supported yet!" ;;
    esac
}


DIR="$1"
for TITLE in "$DIR"/*; do
    [[ "$TITLE" = *.webloc ]] && continue
    [[ -f "${TITLE}.webloc" ]] && continue
    name="$(basename "$TITLE")"
    echo $name
    [[ ! -d "$TITLE" ]] && name=${name%.*}  # Remove extension of file
    V=$(echo "$name" | grep -Po '^.+?\d{4}')  # Keep only until year
    [[ "$V" = '' ]] && V="$name"
    V="$(echo $V | sed 's/[^a-zA-Z0-9]/ /g')"  # Strip out non-alnum
    echo -e "\t$V"
    W="$(/bin/bash "$(dirname "$0")/imdb.sh" "$V")"
    U="$(echo $W | grep -Po '^[^ \"]+')"  # The URL part
    N="$(echo $W | grep -Po '".+"$')"     # The name part
    if [ $? -eq 0 ]; then
        echo -e "\t\t$N"
        echo -e "\t\t$U"
        shortcut "$TITLE" "$U"
    else
        echo -e '\t\tFound nothing.'
    fi
done

shift && [[ "$1" != '' ]] && $0 $* || exit 0
