#awo • [Bitbucket](https://bitbucket.org/fenollp/zwo)




##	Overview

A set of tools to fetch quick info from IMDB effortlessly.  

qlIMDB is a failure, so this project will attempt to do something as easy as
possible. It will browse given folders recursively to find movie files,
find them on IMDB and write a web shortcut in the current directory.  
Then you'll be able to QuickLook it, only hitting your SPACE bar!

### Tools

* **imdb.sh**: `./imdb.sh The Zero Theorem` will give you its IMDB.com address [imdb.com/title/tt2333804](http://imdb.com/title/tt2333804)
* **ql.sh**: `./ql.sh ~/Videos ~/Downloads` will call `imdb.sh` on your files ≥200MB and suffixed by .mkv, .avi, … and then write an Internet shortcut file there.
