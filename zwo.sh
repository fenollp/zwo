#!/bin/bash

# Prompt for URL & use `youtube-dl` to download its film.

while true; do
    read -p "Copie-colle l'adresse de la page contenant la vidéo,
        ou drag & drop un fichier vidéo:
" url
    case "$url" in

        http*)
            echo "C'est parti pour '$url'"
            cd ~/Desktop
            youtube-dl --title "$url"
            echo -e "$(date)\t$url" >> ~/Documents/.dled
            cd -
            break;;

        *)
            if [[ -f "$url" ]]; then
                cd "$(dirname "$url")" \
                    && $HOME/.bin/builds/subdl.git/subdl "$(basename "$url")" \
                    && echo -e "$(date)\t$url" >> ~/Documents/.subed
            else
                echo "'$url' c'est pas une adresse"
            fi

    esac
done
