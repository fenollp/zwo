#!/usr/bin/env python2

import sys
import urllib2
import lxml
from lxml import html

def search (name):
    """search(name): a list of results from TPB's results page."""
    source = search_page(name)
    results = parse(source)
    return results

def best (results):
    """best(results): the best result in the list."""
    pass

def search_page (tpb_query):
    url = 'http://thepiratebay.se/search/' + urllib2.quote(tpb_query)
    response = urllib2.urlopen(url)
    print 'code:', response.getcode(), 'url:', url
    source_code = response.read()
    return source_code

def parse (source_code):
    """parse(source_code"): HTML to dicts convertion."""
    trs = lxml.html.fromstring(source_code).findall('.//tr')
    return [parse_tr(tr) for tr in trs[1:]]

def parse_tr (tr):
    tds = tr.findall('td')
    subtitle = tds[1].find('font').text_content().replace(u'\xa0',' ')
    [date, size, uled] = subtitle.encode('utf-8').split(',')
    return {
        'Type': thin_up(tds[0]),
        'Name': thin_up(tds[1].find('a')),
        'Date': date[len('Uploaded '):],
        'Size': size[len(' Size '):],
        'ULer': uled[len(' ULed by '):],
        'SE': int(thin_up(tds[-2])),
        'LE': int(thin_up(tds[-1])),
        'Magnet': extract_magnet(tr)
    }

def thin_up (elem):
    text = elem.text_content()
    return ' '.join(text.split())

def extract_magnet (tr):
    for a in tr.findall('.//a'):
        href = a.get('href')
        if href.startswith('magnet:?'):
            return href


if __name__ == '__main__':
    results = search(sys.argv[1])
    for r in results:
        print r
